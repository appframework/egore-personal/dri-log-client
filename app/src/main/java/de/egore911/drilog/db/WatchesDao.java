/* @(#)WatchesDao.java
 *
 * Copyright (C) 2011-2012 Christoph Brill
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.egore911.drilog.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Christoph Brill <egore911@gmail.com>
 * @since 0.7
 */
public class WatchesDao {

    private SQLiteDatabase database;

    public void open() throws SQLException {
        database = DbOpenHelper.instance.getWritableDatabase();
    }

    public void close() {
        database.close();
    }

    /**
     * @return the list of watched usernames
     */
    public List<String> getWatches() {
        List<String> watches = new ArrayList<>();

        Cursor c = database.query("watches", new String[]{
                "watch_id"
        }, null,
                null, null, null, "watch_id");
        if (c != null) {
            try (c) {
                if (c.moveToFirst()) {
                    do {
                        watches.add(c.getString(0));
                    } while (c.moveToNext());
                }
            }
        }
        return watches;
    }

    /**
     * Add a username to the persistent list of watched users.
     *
     * @param watch username to be watched
     */
    public void addWatch(String watch) {
        ContentValues values = new ContentValues();
        values.put("watch_id", watch);
        database.insert("watches", null, values);
    }

    /**
     * Remove a username from the persistent list of watched users.
     *
     * @param watch username no longer to be watched
     */
    public void removeWatch(String watch) {
        ContentValues values = new ContentValues();
        values.put("watch_id", watch);
        database.delete("watches", "watch_id=?", new String[]{
                watch
        });
    }

}
