/* @(#)DateUtil.java
 *
 * Copyright (C) 2011-2012 Christoph Brill
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.egore911.drilog.util;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Several helper function related to dates.
 * 
 * @author Christoph Brill <egore911@gmail.com>
 * @since 0.2
 */
public class DateUtil {

    public static class SimpleDate implements Serializable {

        private static final long serialVersionUID = 1L;

        public int year;
        public int month;
        public int day;

        public SimpleDate() {
            final Calendar c = Calendar.getInstance();
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
        }

        public SimpleDate(Date date) {
            final Calendar c = Calendar.getInstance();
            c.setTime(date);
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
        }
    }

    /**
     * @param date date to be converted to a string
     * @return a string in the form "yyyy-mm-dd"
     */
    public static String getDateString(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return getDateString(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * Convert a date to a string without using a DateFormat.
     * 
     * @return a string in the form "yyyy-mm-dd"
     */
    public static String getDateString(@Nonnull SimpleDate date) {
        return getDateString(date.year, date.month, date.day);
    }

    /**
     * Convert a date to a string without using a DateFormat.
     * 
     * @return a string in the form "yyyy-mm-dd"
     */
    public static String getDateString(int year, int month, int day) {
        StringBuilder buffer = new StringBuilder();
        buffer.append(year);
        buffer.append("-");
        if ((month + 1) < 10) {
            buffer.append(0);
        }
        buffer.append((month + 1));
        buffer.append("-");
        if (day < 10) {
            buffer.append(0);
        }
        buffer.append(day);
        return buffer.toString();
    }

    /**
     * @param datestring a string in the form "yyyy-mm-dd"
     */
    public static Date getDate(@Nonnull String datestring) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, Integer.parseInt(datestring.substring(0, 4)));
        cal.set(Calendar.MONTH,
                Integer.parseInt(datestring.substring(5, 7)) - 1);
        cal.set(Calendar.DAY_OF_MONTH,
                Integer.parseInt(datestring.substring(8, 10)));
        return cal.getTime();
    }

}
