/* @(#)Constants.java
 *
 * Copyright (C) 2011-2014 Christoph Brill
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.egore911.drilog.util;

/**
 * Several constants used within the client
 *
 * @author Christoph Brill <egore911@gmail.com>
 * @since 0.7
 */
public class Constants {

    /**
     * This URL is where the DRI-log server is hosted
     */
    public static final String BASE_URL = "https://people.freedesktop.org/~cbrill/dri-log/";

    /**
     * The hardcoded list of channels available at the time of release of the app
     */
    public static final String[] DEFAULT_CHANNELS = new String[]{"dri-devel", "radeon", "nouveau", "wayland", "d3d9"};

    public static final String INTENT_CHANNELS = "channels";
    public static final String STATE_DATE = "date";
}
