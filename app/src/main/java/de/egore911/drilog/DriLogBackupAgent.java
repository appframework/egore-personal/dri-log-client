/* DriLogBackupAgent.java
 *
 * Copyright (C) 2011-2012 Christoph Brill
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.egore911.drilog;

import android.app.backup.BackupAgent;
import android.app.backup.BackupDataInput;
import android.app.backup.BackupDataOutput;
import android.os.ParcelFileDescriptor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.egore911.drilog.db.WatchesDao;

/**
 * @author Christoph Brill <egore911@gmail.com>
 * @since 0.6
 */
public class DriLogBackupAgent extends BackupAgent {

    private static final String WATCHES_BACKUP_KEY = "watches";
    private static final String SEPARATOR = ";;;;;";
    private WatchesDao mWatchesDao;

    @Override
    public void onBackup(ParcelFileDescriptor oldState, BackupDataOutput data, ParcelFileDescriptor newState) throws IOException {
        mWatchesDao = new WatchesDao();
        mWatchesDao.open();
        try {
            FileInputStream instream = new FileInputStream(oldState.getFileDescriptor());
            DataInputStream in = new DataInputStream(instream);

            // Read the previously watched users
            List<String> previous;
            try {
                previous = split(in.readUTF());
            } catch (EOFException e) {
                previous = new ArrayList<>(0);
            }

            // Read the currently watched users
            List<String> current;
            current = mWatchesDao.getWatches();
            String watches = join(current);

            // If they differ ...
            if (!previous.containsAll(current) || !current.containsAll(previous)) {
                ByteArrayOutputStream bufStream = new ByteArrayOutputStream();
                DataOutputStream outWriter = new DataOutputStream(bufStream);

                outWriter.writeUTF(watches);

                byte[] buffer = bufStream.toByteArray();
                int len = buffer.length;
                data.writeEntityHeader(WATCHES_BACKUP_KEY, len);
                data.writeEntityData(buffer, len);
            }

            FileOutputStream outstream = new FileOutputStream(newState.getFileDescriptor());
            DataOutputStream out = new DataOutputStream(outstream);

            out.writeUTF(watches);
        } finally {
            mWatchesDao.close();
        }
    }

    @Override
    public void onRestore(BackupDataInput data, int appVersionCode, ParcelFileDescriptor newState) throws IOException {
        mWatchesDao = new WatchesDao();
        mWatchesDao.open();
        try {
            String watches = null;
            while (data.readNextHeader()) {
                String key = data.getKey();
                int dataSize = data.getDataSize();

                if (WATCHES_BACKUP_KEY.equals(key)) {
                    byte[] dataBuf = new byte[dataSize];
                    data.readEntityData(dataBuf, 0, dataSize);
                    ByteArrayInputStream baStream = new ByteArrayInputStream(dataBuf);
                    DataInputStream in = new DataInputStream(baStream);

                    watches = in.readUTF();

                    // Set the watchlist
                    setWatches(watches);

                } else {
                    // We don't know this entity key. Skip it. (Shouldn't happen.)
                    data.skipEntityData();
                }
            }

            // Finally, write to the state blob (newState) that describes the restored data
            FileOutputStream outstream = new FileOutputStream(newState.getFileDescriptor());
            DataOutputStream out = new DataOutputStream(outstream);
            out.writeUTF(watches);
        } finally {
            mWatchesDao.close();
        }
    }

    private void setWatches(String watches) {
        List<String> previous = mWatchesDao.getWatches();

        for (String w : split(watches)) {
            if (previous.contains(w)) {
                continue;
            }
            mWatchesDao.addWatch(w);
        }

        for (String w : previous) {
            if (watches.contains(w)) {
                continue;
            }
            mWatchesDao.removeWatch(w);
        }
    }

    private String join(List<String> watchList) {
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < watchList.size(); i++) {
            if (i > 0) {
                buffer.append(SEPARATOR);
            }
            String watch = watchList.get(i);
            buffer.append(watch);
        }
        return buffer.toString();
    }

    private List<String> split(String s) {
        return Arrays.asList(s.split(SEPARATOR));
    }
}
