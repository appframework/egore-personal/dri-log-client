package de.egore911.drilog.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;

public class DateUtilTest {

    private static final DateUtil.SimpleDate SIMPLEDATE_FIRST_JAN_1970 = new DateUtil.SimpleDate();
    private static final DateUtil.SimpleDate SIMPLEDATE_TODAY = new DateUtil.SimpleDate();
    private static final DateUtil.SimpleDate SIMPLEDATE_TODAY_ALT = new DateUtil.SimpleDate(new Date());

    private static final Date DATE_TODAY = new Date();

    static {
        SIMPLEDATE_FIRST_JAN_1970.day = 1;
        SIMPLEDATE_FIRST_JAN_1970.month = 0;
        SIMPLEDATE_FIRST_JAN_1970.year = 1970;
    }

    @Test
    public void testSimpleDateGetDateStringNull() {
        Assertions.assertThrows(NullPointerException.class, () -> DateUtil.getDateString((DateUtil.SimpleDate) null));
    }

    @Test
    public void testSimpleDateGetDateStringFirstJan1970() {
        Assertions.assertEquals(DateUtil.getDateString(SIMPLEDATE_FIRST_JAN_1970),"1970-01-01");
    }

    @Test
    public void testSimpleDateGetDateStringToday() {
        Assertions.assertFalse(DateUtil.getDateString(SIMPLEDATE_TODAY).isEmpty());
    }

    @Test
    public void testSimpleDateGetDateStringTodayDate() {
        Assertions.assertFalse(DateUtil.getDateString(SIMPLEDATE_TODAY_ALT).isEmpty());
    }

    @Test
    public void testDateGetDateStringNull() {
        Assertions.assertThrows(NullPointerException.class, () -> DateUtil.getDateString((Date) null));
    }

    @Test
    public void testDateGetDateStringToday() {
        Assertions.assertFalse(DateUtil.getDateString(DATE_TODAY).isEmpty());
    }

}
